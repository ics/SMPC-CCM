# [Predictive control for nonlinear stochastic systems: Closed-loop guarantees with unbounded](https://arxiv.org/abs/2407.13257) - code for the paper 

# Dependencies
- [YALMIP](https://yalmip.github.io/) for offline computations
- [CasADi](https://web.casadi.org/) + [IPOPT](https://coin-or.github.io/Ipopt/) for solving nonlinear MPC problems

# Model
`Model.m` characterizes the nonlinear system dynamics and noise distributions
# Offline computations
Run `Offline_design.m` to compte the contraction metric and save it in `offline.mat`
# Probabilistic reachable set
Run `PRS_simulations.m` to check expected error and probabilistic containement for different inputs and distributions and save it in `Results/PRS_sim.mat`
# Open-loop Comparison
Run `Comparison_SamplingBased.m` to check compare proposed approach to sampling-based approaches and robust-stochastic predictive control approaches for stochastic open-loop optimal control problem and save it in `Results/ComparisonSampling.mat`
# Closed-loop SMPC simulations
Run `SMPC_simulations.m` simulate closed-loop system with stochastic MPC and save it in `Results/closed-loop.mat`
(Running this for the 10^5 samples may take multiple days, consider reducing N_samples=10^3 for quick tests.)
To get the computational times, set the flag computational_comparison=true in the same file.
# Plots 
Run `MakePlots.m` to generate the figures which are saved in folder `Figures`.