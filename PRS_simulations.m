%% 
%This file simulates open-loop trajectories to validate the error bounds of
%the Contraction metric
clear all
close all
clc
load offline%load offline computation
Model=Model;%load dynamics model
%% 
N_samples=1e5;%number of random samples
T_sim=1e3;%simulation times
input_modes=2;%different inputs
disturbance_modes=6+1;%different distributions
%pre-allocate
error_M=zeros(T_sim+1,input_modes,disturbance_modes);  
error_percentile=zeros(T_sim+1,input_modes,disturbance_modes);
empirical_prob=ones(T_sim+1,input_modes,disturbance_modes);
PRS_bound=zeros(T_sim+1,1);
for k=1:T_sim+1
%compute bound based on theory
PRS_bound(k)=obj.bar_w/(1-obj.rho)*(1-obj.rho.^(k-1))/(1-obj.probability_level);
end
t=tic;
for u_mode=1:input_modes%which input
for w_mode=1:disturbance_modes%which noise distribution
    [u_mode,w_mode]%to see progress
    if u_mode==1
    %zero input (operating close to origin)
    u=zeros(obj.n_u,N_samples,T_sim);     
    else
    %periodic forcing
    u=zeros(obj.n_u,N_samples,T_sim); 
        for k=1:T_sim
            u(:,:,k)=1e3*ones(obj.n_u,N_samples)*sin(k*2*pi/50);
        end 
    end
    error=zeros(N_samples,T_sim+1);
    w=zeros(obj.n_w,N_samples,T_sim);
    x=zeros(obj.n_x,N_samples,T_sim+1);
    z=x;
    for k=1:T_sim
        switch w_mode 
            case 1
            w(:,:,k)=mvnrnd(zeros(obj.n_w,1)',obj.Sigma_w,N_samples)';%Gaussian
            case 2
            w(:,:,k)=Model.distribution_worstcase(0.99,obj.Sigma_w,N_samples);%worst case; but with different probability (few outliers)
            case 3
            w(:,:,k)=Model.distribution_worstcase(0.995,obj.Sigma_w,N_samples);
            case 4
            w(:,:,k)=Model.distribution_worstcase(0.999,obj.Sigma_w,N_samples); 
            case 5
            w(:,:,k)=Model.distribution_worstcase(0.9995,obj.Sigma_w,N_samples); 
            case 6
            w(:,:,k)=Model.distribution_worstcase(0.9999,obj.Sigma_w,N_samples); 
            case 7
            w(:,:,k)=Model.log_distribution(obj.Sigma_w,N_samples);    
        end
        %simlate (multiSim makes it parallelized)
        x(:,:,k+1)=Model.multiSim(x(:,:,k),u(:,:,k),w(:,:,k),obj.dt,N_samples);
        z(:,k+1)=Model.dyn(z(:,k),u(:,1,k),zeros(obj.n_w,1),obj.dt);
        temp=reshape(x(:,:,k+1),[obj.n_x,N_samples])-repmat(z(:,k+1),1,N_samples);
        for i=1:N_samples
          error(i,k+1)=temp(:,i)'*obj.M*temp(:,i);%quadratic error
        end
        error_M(k+1,u_mode,w_mode)=mean(error(:,k+1));%compute expected error

        error_sorted=sort(error(:,k+1));%sort error
        error_percentile(k+1,u_mode,w_mode)=error_sorted(obj.probability_level*N_samples);
        %check probability containement
        empirical_prob(k+1,u_mode,w_mode)=mean(error(:,k+1)<=PRS_bound(k+1));

    end
end
end
toc(t)
save('PRS_sim','error_M','T_sim','error_percentile','empirical_prob','N_samples')


 