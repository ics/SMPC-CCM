%% Comparison
%This files compared the proposed approach to a nominal implementation; a
%different SMPC formulation, and sampling-based approximations;
%with a finite-horizon open-loop optimal control problem. 
%First, the different SMPC problems are formulated and the computational
%times are recorded. 
%Then, the input is applied to the system under independently sampled
%random noise realizations to compute stochastic quantities
%%
clear all
close all
clc
%load offline design
load Results/offline
Model=Model;
%% 1. sample random noise
%set number of samples in MPC for sampling-baaed approximations
Ns=[10,20,50,1e2,2e2,5e2,1e3];
N_solves=1e2;%use multiple solves for average comp. times to reduce effect of overhead (not perfect, but ball-park numbers); consistent with script SMPC_simulations.m
w_mode=1;% set: discrete,normal, log-normal distribution
p_w=0.99;%defines discrete distribution, corresponds to (ii) in Figure
import casadi.*
obj.N=15; %horizon
x0=zeros(obj.n_x,1);x0(5)=10;%only large position in last cart
x_init=x0;  
%pre-allocte
u_opt_samples=zeros(obj.N,length(Ns));
t_sampling=zeros(length(Ns),1);
cost_OL_sampling=zeros(length(Ns),1);
%% solve sampling-bsaed SMPC
for i=1:length(Ns)%loop through number of samples
obj.Ns=Ns(i);
rng("default")%fix random seed for reproducability 
switch w_mode %sample noise
       case 1
        w_samples=Model.distribution_worstcase(p_w,kron(eye(obj.Ns),obj.Sigma_w),obj.N);
       case 2
        w_samples=mvnrnd(zeros(obj.Ns*obj.n_w,1),kron(eye(obj.Ns),obj.Sigma_w),obj.N)'; 
       case 3 
        w_samples=Model.log_distribution(kron(eye(obj.Ns),obj.Sigma_w),obj.N);
end 
%
x_samples=MX.sym('x',(obj.N+1)*obj.n_x*obj.Ns);%mean
u_sampling=MX.sym('u',obj.N*obj.n_u);%input
y_sampling=[x_samples;u_sampling];%stacked decision variable
cost_sampling = MX(0);
cost_sampling=costfunction_sampling(obj,y_sampling);
[c_sampling, ceq_sampling] = constraints_sampling(obj,y_sampling,w_samples,Model); %no terminal constraint
con_sampling=[ceq_sampling;c_sampling]; 
con_sampling_lb=[zeros(obj.N*obj.n_x*obj.Ns,1);-inf(size(obj.h_k,1)*obj.N*obj.Ns,1)];
con_sampling_ub=[zeros(obj.N*obj.n_x*obj.Ns,1);repmat(obj.h_k(:,1),obj.Ns*obj.N,1)];
lb_sampling=[-inf(size(x_samples));repmat(obj.u_min,obj.N,1)];
ub_sampling=[inf(size(x_samples));repmat(obj.u_max,obj.N,1)];  
lb_sampling(1:obj.n_x*obj.Ns)=repmat(x_init,obj.Ns,1);
ub_sampling(1:obj.n_x*obj.Ns)=repmat(x_init,obj.Ns,1);
nlp_sample = struct('x', y_sampling, 'f', cost_sampling, 'g', con_sampling);
%set up NLP
opts=struct;
opts.print_time=0;
opts.ipopt.print_level=1;
solver_samples = nlpsol('solver', 'ipopt', nlp_sample,opts); 
%time solver
t=tic;
for j=1:N_solves
res = solver_samples('lbx', lb_sampling,...           % lower bound on x
                     'ubx', ub_sampling,...           % upper bound on x
                     'lbg', con_sampling_lb,...           % lower bound on g
                     'ubg', con_sampling_ub);             % upper bound on g
end
y_opt=full(res.x); 
t_sampling(i)=toc(t)/N_solves;
cost_OL_sampling(i)=full(res.f);
%check feaibility
[c_opt,c_eq_opt]=constraints_sampling(obj,y_opt,w_samples,Model);
if norm(c_eq_opt)>1e-5 || norm(max(c_opt-repmat(obj.h_k(:,1),obj.Ns*obj.N,1),0))>1e-5
  error('solution not feasible to provided tolerance');
  i=i
  j=j
end
u_opt_samples(:,i)=y_opt((obj.N+1)*obj.n_x*obj.Ns+1:end);
end
%% solve nominal MPC
x_nom=MX.sym('x',(obj.N+1)*obj.n_x);%mean
u_nom=MX.sym('u',obj.N*obj.n_u);%input
y_nom=[x_nom;u_nom];
y_nom_ext=[y_nom(1:(obj.N+1)*obj.n_x);y_nom];
cost_nom = MX(0);
cost_nom=costfunction(obj,y_nom_ext);
[c_nom, ceq_nom] = nonlinearconstraints_nom(obj,y_nom,Model); 
con_nom=[ceq_nom;c_nom];
con_bound_nom=zeros(obj.N*obj.n_x,1);%dynamic equality
con_lb_nom=[con_bound_nom;-inf(size(obj.h_k,1)*obj.N,1)];
con_ub_nom=[con_bound_nom;repmat(obj.h_k(:,1),obj.N,1)];%mixed inear state inequality constraints
lb_nom=[-inf(size(x_nom));repmat(obj.u_min,obj.N,1)];
ub_nom=[inf(size(x_nom));repmat(obj.u_max,obj.N,1)];
%add initial state constraint
lb_nom(1:obj.n_x)=x_init;
ub_nom(1:obj.n_x)=x_init;
nlp_nom = struct('x', y_nom, 'f', cost_nom, 'g', con_nom);
solver_nom = nlpsol('solver', 'ipopt', nlp_nom,opts); 
       
t=tic;
for j=1:N_solves
res = solver_nom('lbx', lb_nom,'ubx', ub_nom,'lbg', con_lb_nom,'ubg', con_ub_nom);         
end
y_opt=full(res.x); 
t_nom=toc(t)/N_solves; 
u_nom=y_opt((obj.N+1)*obj.n_x+1:end);
cost_OL_nom=full(res.f);
%% solve proposed SMPC
z=MX.sym('z',(obj.N+1)*obj.n_x);%nominal 
x=MX.sym('x',(obj.N+1)*obj.n_x);%mean
u=MX.sym('u',obj.N*obj.n_u);%input
y_dec=[z;x;u];%stacked decision variable
cost_optimized = MX(0);
cost_optimized=costfunction(obj,y_dec);
[c, ceq] = nonlinearconstraints(obj,y_dec,Model); 
con=[ceq;c];
con_bound=zeros(obj.N*obj.n_x*2,1);%dynamic equality
con_lb=[con_bound;-inf(size(obj.h_k,1)*obj.N,1)];
k=1;%set initial time (since constraints are shifted)
z_init=x_init;%initial conditions coincide at k=0
con_ub=[con_bound;reshape(obj.h_k(:,k:k+obj.N-1),size(obj.h_k,1)*obj.N,1)];%mixed linear state inequality constraints 
lb=[-inf(size(z));-inf(size(x));repmat(obj.u_min,obj.N,1)];
ub=[inf(size(z));inf(size(x));repmat(obj.u_max,obj.N,1)];
%add initial state constraint
lb(1:obj.n_x)=z_init;
ub(1:obj.n_x)=z_init;
lb(obj.n_x*(obj.N+1)+1:obj.n_x*(obj.N+1)+obj.n_x)=z_init;
ub(obj.n_x*(obj.N+1)+1:obj.n_x*(obj.N+1)+obj.n_x)=z_init;
%optimization
nlp = struct('x', y_dec, 'f', cost_optimized, 'g', con);
solver_proposed = nlpsol('solver', 'ipopt', nlp,opts); 
t=tic;
for j=1:N_solves
res = solver_proposed('lbx', lb,'ubx', ub,'lbg', con_lb,'ubg', con_ub);         
end
y_opt=full(res.x); 
t_proposed=toc(t)/N_solves; 
u_proposed=y_opt((obj.N+1)*obj.n_x*2+1:end);
cost_OL_proposed=full(res.f);
%% solve robust-stochastic MPC
%uses formulation by [Schluter&Allogwer 2020], with V_delta computed using
%contraction metrics.
if w_mode~=1
    error('this formulation is only defined for bounded support')
end 
w_test=Model.distribution_worstcase(p_w,obj.Sigma_w,1e6);
M_sqrt=sqrtm(obj.M);
w_robust=0;
[~,~,G_fun,~] = Model.get_jacobian(obj,obj.dt);
G= full(value(G_fun(zeros(obj.n_x,1),zeros(obj.n_u,1),zeros(obj.n_w,1))));%this uses the fact that G is constant in the considered example
w_prob=zeros(length(w_test),1);
for i=1:size(w_test,2)
    %worst case disturbance
    w_robust=max(w_robust,norm(M_sqrt*G*w_test(:,i),2));
    %probabilistic one-step bound
    w_prob(i)=norm(M_sqrt*G*w_test(:,i),2);
end
w_prob=sort(w_prob);
w_prob=w_prob(round(obj.probability_level*length(w_prob)));%in this example, most disturbances are zero, hence this is zero...s
%propagation: 
rho_robust=sqrt(obj.rho);%work with |x-z|_M (not square) --> contraction rate gets square-root
%construct tightened constraints
w_robust/(1-rho_robust).*obj.c_j;%this term corresponds to the tightening for k->infty and needs to be <1 for feasibility with large horizons
h_robust_k=repmat(obj.h_k(:,1),1,obj.N);
for k=1:obj.N 
h_robust_k(:,k)=obj.h_k(:,1)-obj.c_j*(w_prob+w_robust*(1-rho_robust^(k-2))/(1-rho_robust));
end
%use nominal, but with changed constraint
con_ub_robust=[con_bound_nom;reshape(h_robust_k,size(h_robust_k,2)*size(h_robust_k,1),1)];%mixed inear state inequality constraints
t=tic;
for j=1:N_solves
res = solver_nom('lbx', lb_nom,'ubx', ub_nom,'lbg', con_lb_nom,'ubg', con_ub_robust);         
end
y_opt=full(res.x); 
t_robust=toc(t)/N_solves; 
u_robust=y_opt((obj.N+1)*obj.n_x+1:end);
cost_OL_robust=full(res.f);
%% simulate response
N_samples=1e5;
w_validate=zeros(obj.n_w,N_samples,obj.N);
violation=zeros(N_samples,obj.N,length(Ns));
con_plot=zeros(N_samples,obj.N,length(Ns));
%generate disturbances for validation
rng("default")%fix random seed for reproducability
Model.distribution_worstcase(p_w,obj.Sigma_w,obj.N*sum(Ns));%call onces to not get exact same samples as in MPC design
for i=1:N_samples
  switch w_mode
       case 1
        w_validate(:,i,:)=Model.distribution_worstcase(p_w,obj.Sigma_w,obj.N);
       case 2
        w_validate(:,i,:)=mvnrnd(zeros(obj.n_w,1),obj.Sigma_w,obj.N)'; 
       case 3 
        w_validate(:,i,:)=Model.log_distribution(obj.Sigma_w,obj.N);
   end
end
%%
cost_traj=zeros(N_samples,length(Ns));
%
for j=1:length(Ns)+3%loop over different inputs (nominal+proposed+robust+sampling)
if j==1
u_applied=u_nom;
elseif j==2
u_applied=u_proposed;
elseif j==3
u_applied=u_robust;
else
u_applied=u_opt_samples(:,j-3);
end
x_validate=zeros(obj.n_x,N_samples,obj.N+1);
x_validate(:,:,1)=repmat(x_init,1,N_samples);
for i=1:N_samples
cost_traj(i,j)=0;
for k=1:obj.N
    x_validate(:,i,k+1)=Model.dyn(x_validate(:,i,k),u_applied(k),w_validate(:,i,k),obj.dt);
    con_temp=obj.H*x_validate(:,i,k)-obj.h_k(:,1);
    cost_traj(i,j)=cost_traj(i,j)+runningcosts(x_validate(:,i,k), u_applied(k),zeros(obj.n_x,1), 0, obj.Q,obj.R);
    violation(i,k,j)=max(con_temp)>1e-7;
    con_plot(i,k,j)=con_temp(end);
end
   %terminal cost (also optimized) 
    cost_traj(i,j)=cost_traj(i,j)+runningcosts(x_validate(:,i,obj.N+1),0,zeros(obj.n_x,1), 0, obj.P,0*obj.R);
end 
end
%% extract numbers
%probabiltiy
violation_prob=zeros(obj.N,length(Ns));
for j=1:length(Ns)+3%loop over different inputs
 for k=1:obj.N
   violation_prob(k,j)=mean(violation(:,k,j));   
 end
end
plot(violation_prob)
hold on
plot([0,obj.N],(1-obj.probability_level)*[1,1])
legend('nom','proposed','robust','$10$','$20$','$50$','$100$','$200$')
%% Summarize main quantities
Ns
Probability=round(max(violation_prob)*1e2,1)
t_solve=round([t_nom,t_proposed,t_robust,t_sampling']*1e3,0)
average_cost=round(mean(cost_traj),0)
%cost_OL=[cost_OL_nom,cost_OL_proposed,cost_OL_robust,cost_OL_sampling./Ns]%open-loop
%cost; not easy to interpret in comparison to sampling-based.
save('Results/ComparisonSampling','Probability','average_cost','Ns','N_samples','t_solve','N_solves','p_w')
%%
function cost=costfunction_sampling(param,y_dec)
        %open-loop cost for decision variable y_dec
        x=y_dec(1:(param.N+1)*param.n_x*param.Ns);
        u=y_dec((param.N+1)*param.n_x*param.Ns+1:end);
         cost = 0;
        for k=1:param.N
           x_k=x(param.n_x*param.Ns*(k-1)+1:param.n_x*param.Ns*k);
           u_k=u(param.n_u*(k-1)+1:param.n_u*k);
           for j=1:param.Ns
           x_kj=x_k(param.n_x*(j-1)+1:j*param.n_x);
           cost = cost + runningcosts(x_kj, u_k,0*x_kj,0*u_k,param.Q, param.R); 
           end
        end
        %terminal
        x_N=x(param.n_x*param.Ns*param.N+1:param.n_x*param.Ns*(param.N+1));
        for j=1:param.Ns
               x_Nj=x_N(param.n_x*(j-1)+1:j*param.n_x);
           cost = cost + runningcosts(x_Nj, u_k,0*x_kj,0*u_k,param.P, 0); 
        end
end

function cost = runningcosts(x, u, x_eq, u_eq, Q, R)
            % Provide the running cost   
            cost = (x-x_eq)'*Q*(x-x_eq) + (u-u_eq)'*R*(u-u_eq);
end

function [c, ceq] = constraints_sampling(param,y_dec,w_samples,Model) 
        % Introduce the nonlinear constraints also for the terminal state  
        x=y_dec(1:(param.N+1)*param.n_x*param.Ns);
        u=y_dec((param.N+1)*param.n_x*param.Ns+1:end);
        c = [];
        ceq = [];    
        % constraints along prediction horizon
            for k=1:param.N
             x_k=x(param.n_x*param.Ns*(k-1)+1:param.n_x*param.Ns*k);
             u_k=u(param.n_u*(k-1)+1:param.n_u*k);
             w_k=w_samples(:,k);
             x_new=x(param.n_x*param.Ns*k+1:param.n_x*param.Ns*(k+1));
            %for each sample
            for j=1:param.Ns
                w_kj=w_k(param.n_w*(j-1)+1:j*param.n_w);
                x_kj=x_k(param.n_x*(j-1)+1:j*param.n_x);
                x_newj=x_new(param.n_x*(j-1)+1:j*param.n_x);
                %dynamic constraint x,z:
                ceqnew=x_newj - Model.dyn(x_kj, u_k,w_kj,param.dt);
                ceq = [ceq; ceqnew];
                %nonlinear constraints on state and input could be included here 
                cnew=param.H*x_kj;%(linear) state constraints (not just box, due to difference position)
                c=[c;cnew];
            end  
            end
             %terminal set constraint: |z_N|_M^2\leq \alpha_f
            x_N=x(param.n_x*param.Ns*param.N+1:param.n_x*param.Ns*(param.N+1));
            for j=1:param.Ns
            x_Nj=x_N(param.n_x*(j-1)+1:j*param.n_x);
            %no terminal constraint
            %cnew=[runningcosts(x_N,0,0*x_N,0,param.M,0)-param.alpha_f];%<=0 ensures terminal
            %c=[c;cnew];
            end
end
function [c, ceq] = nonlinearconstraints_nom(param,y_dec,Model) 
        % Introduce the nonlinear constraints also for the terminal state  
        x=y_dec(1:(param.N+1)*param.n_x);
        u=y_dec((param.N+1)*param.n_x+1:end); 
        c = [];
        ceq = [];
        % constraints along prediction horizon
            for k=1:param.N
                x_k=x(param.n_x*(k-1)+1:param.n_x*k);
                u_k=u(param.n_u*(k-1)+1:param.n_u*k);
                x_new=x(param.n_x*k+1:param.n_x*(k+1));
                %dynamic constraint x,z:
                ceqnew=x_new - Model.dyn(x_k, u_k,zeros(param.n_w,1),param.dt);
                ceq = [ceq; ceqnew];
                %nonlinear constraints on state and input could be included here 
                cnew=param.H*x_k;%(linear) state constraints (not just box, due to difference position)
                c=[c;cnew];
            end
            %no terminal set constraint here for comparison
            %terminal set constraint: |z_N|_M^2\leq \alpha_f
            %x_N=x(param.n_x*param.N+1:param.n_x*(param.N+1));
            %cnew=[runningcosts(x_N,0,0*x_N,0,param.M,0)-param.alpha_f];%<=0 ensures terminal
            %c=[c;cnew];
end
 

function cost=costfunction(param,y_dec)
%cost, proposed formulation using predictions x and z
        %open-loop cost for decision variable y_dec
        x=y_dec((param.N+1)*param.n_x+1:(param.N+1)*param.n_x*2);
        u=y_dec((param.N+1)*param.n_x*2+1:end);
         cost = 0;
        for k=1:param.N
           x_k=x(param.n_x*(k-1)+1:param.n_x*k);
           u_k=u(param.n_u*(k-1)+1:param.n_u*k);
           cost = cost + runningcosts(x_k, u_k,0*x_k,0*u_k,param.Q, param.R); 
        end
        %terminal:
           x_N=x(param.n_x*param.N+1:param.n_x*(param.N+1));
           cost = cost + runningcosts(x_N, 0,0*x_N,0,param.P,0); 
end
function [c, ceq] = nonlinearconstraints(param,y_dec,Model)
%nonlinear constraints for proposed formulation
        z=y_dec(1:(param.N+1)*param.n_x);
        x=y_dec((param.N+1)*param.n_x+1:(param.N+1)*param.n_x*2);
        u=y_dec((param.N+1)*param.n_x*2+1:end);
        c = [];
        ceq = [];  
        %last variable is nominal initial state
        %-> make x trajectory nominal 
        c = [];
        ceq = [];
        % constraints along prediction horizon
            for k=1:param.N
                x_k=x(param.n_x*(k-1)+1:param.n_x*k);
                u_k=u(param.n_u*(k-1)+1:param.n_u*k);
                x_new=x(param.n_x*k+1:param.n_x*(k+1));
                z_k=z(param.n_x*(k-1)+1:param.n_x*k);
                z_new=z(param.n_x*k+1:param.n_x*(k+1));
                %dynamic constraint x,z:
                ceqnew=x_new - Model.dyn(x_k, u_k,zeros(param.n_w,1),param.dt);
                ceq = [ceq; ceqnew];
                ceqnew=z_new - Model.dyn(z_k, u_k,zeros(param.n_w,1),param.dt);
                ceq = [ceq; ceqnew];
                %nonlinear constraints on state and input could be included here 
                cnew=param.H*z_k;%(linear) state constraints (not just box, due to difference position)
                c=[c;cnew];
            end
            %no terminal set constraints in comparison here
            %terminal set constraint: |z_N|_M^2\leq \alpha_f
            % z_N=z(param.n_x*param.N+1:param.n_x*(param.N+1));
            %cnew=[runningcosts(z_N,0,0*z_N,0,param.M,0)-param.alpha_f];%<=0 ensures terminal
            %c=[c;cnew];
end