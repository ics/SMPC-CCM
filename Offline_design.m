%% Offline design
%This file sets the parameters and does the offline computation of
%contraction metric, terminal set/cost, and tightened constriants
%%
clear all
close all
clc
t1=tic;%to measure time
%% Specifc problem
obj=struct;
N_masses=3;%number of carts (currently hard coded in Model.m
obj.n_x=2*N_masses;obj.n_u=1;obj.n_w=1*N_masses;%number of states,inpts, disturbances
dt=0.25;%discreteization time
probability_level=0.95;%desired probability
%stage cost
Q=eye(obj.n_x);R=1e-4*eye(obj.n_u);
%% constraints
%max velocity
H=kron(eye(N_masses),[0,1]);
v_max=2;
h=[v_max*ones(N_masses,1)];
%position constraint
distance_min=1;
H=[H;[-1,0,zeros(1,(N_masses-1)*2)]];
for i=1:N_masses-1
H=[H;[zeros(1,(i-1)*2),1,0,-1,0,zeros(1,(N_masses-i-1)*2)]];
end
h=[h;ones(N_masses,1)*distance_min];
%input constraint
obj.u_max=1e2;
obj.u_min=-obj.u_max;

Model=Model;%load model definitions

%define Jacobian 
[A_fun,B_fun,G_fun,f_fun] = Model.get_jacobian(obj,dt);
%% 
%define optimization variables
W=sdpvar(obj.n_x);
X=sdpvar(obj.n_w);
Sigma_w=1e-3*eye(obj.n_w);%noise covariance
ineq=[];
ineq=[ineq,W>=0*eye(obj.n_x)];%ensure positive semi-definite
for i=1:size(H,1)
ineq=[ineq,[W,(H(i,:)*W)';H(i,:)*W,1]>=0];%tightening
end
rho=0.9951;%this constant needs to be tuned as a hyperparameter; e.g., here based on the maximal eigenvalue of linearization
%For this specific system, it's easy to see that the Jacobian only depends
%on the velocity. 
%Looking also at the plot of the Coulomb-friction, it's easy to see that the
%derivative is bounded between the friction at x=0, and the derivation at
%|x|->infty. The simplest way to code this is here taking a very large
%value, as it makes the code easy to read, but can equally be coded by
%noting the bound on the derivate is between linearization at origin and 0.
for v1=[0,1e6] 
for v2=v1+[0,1e6]
for v3=v2+[0,1e6]
    %this "gridding" uses special structure of nonlinearity, with extreme jacobians at x=0 and x->infty        
    x=[0;v1;0;v2;0;v3];u=0;w=0;%since A only depends on v, the rest are set to zero
    A=full(A_fun(x,u,w));%get Jacobian 
    B=full(B_fun(x,u,w));
    G=full(G_fun(x,u,w));
    ineq=[ineq,[X,(G*sqrtm(Sigma_w))';G*sqrtm(Sigma_w),W]>=0];%constraint on variance
    ineq=[ineq,[rho*W,(A*W)';A*W,W]>=0];%contraction constraint
end
end
end 
optimize(ineq,trace(X)/(1-rho))%objective from Appendix B
%save results
bar_w=trace(value(X));
W=value(W);
M=inv(W);
bar_w/(1-rho)/(1-probability_level); 
%% Compute tightened constraints
%Hz<=h_k; for k=1..T_sim
T_sim=5e3;
h_k=zeros(length(h),T_sim);
%tightening constant for each constriants
c_j=0*h;
for i=1:length(h)
    c_j(i)=norm(inv(sqrtm(M))*H(i,:)',2);
end

c=bar_w*(1-probability_level);
%infinite-time
h_inf=h-c_j*sqrt(bar_w/((1-rho)*(1-probability_level)));
for k=1:T_sim 
h_k(:,k)=h-c_j*sqrt(bar_w*(1-rho^(k-1))/((1-rho)*(1-probability_level)));
end

%% Terminal
%terminal cost P:
P=max(eig(Q/M))/(1-rho)*M;
%terminal set - |x|_M^2\leq alpha_f
obj.alpha_f=sqrt(min(h_inf./c_j));
toc(t1)
obj.Q=Q;
obj.R=R;
obj.P=P;
obj.dt=dt;
obj.M=M;
obj.H=H;%state constraints  
obj.h_k=h_k;
obj.c_j=c_j;
obj.bar_w=bar_w;
obj.rho=rho;
obj.probability_level=probability_level;
obj.Sigma_w=Sigma_w;
save('Offline','obj')