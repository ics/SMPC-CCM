


classdef Model
   methods
        function F=F_columb(obj,v)
        %compute columb friction
        d1=1;d2=2*5; 
        F=d1*tanh(d2*v);
        end
        function F=linear_spring_damper(obj,p,v)
        %encode linear spring + friction based on position+velocity 
        d0=1; %damping constant
        k0=2*1;%spring constant
        F_spring=k0*p; 
        F_damping=d0*v; 
        F=F_spring+F_damping;
        end    
        function f=fun(obj,x,u,w)
        %reduce input dimension to one/map scalar->3
        u=[0;0;u];
        %continous time dynamics for M=3
        %first mass attached to wall, last free
        m=1*5; %mass
        p1=x(1);v1=x(2);
        p2=x(3);v2=x(4);
        p3=x(5);v3=x(6);
        %connecting forces between masses (linear)
        F1=linear_spring_damper(obj,p1,v1);
        F2=linear_spring_damper(obj,p2-p1,v2-v1);
        F3=linear_spring_damper(obj,p3-p2,v3-v2);
        %columb friction accting on direct velocity of each cart
        Friction1=F_columb(obj,v1);
        Friction2=F_columb(obj,v2);
        Friction3=F_columb(obj,v3);

        f1=[v1;-1/m*(F1-F2+Friction1-u(1)-w(1))];%
        f2=[v2;-1/m*(F2-F3+Friction2-u(2)-w(2))];%
        f3=[v3;-1/m*(F3+Friction3-w(3)-u(3))];%
        f=[f1;f2;f3];
        end
        function [A_fun,B_fun,G_fun,f_fun] = get_jacobian(obj,param,dt)
              import casadi.*
              x_sym = MX.sym('x',param.n_x,1);
              u_sym = MX.sym('u',param.n_u,1);
              w_sym = MX.sym('w',param.n_w,1);
              f_c =fun(obj,x_sym,u_sym,w_sym);
              if dt == 0
                f = f_c;
              else
                f = dyn(obj,x_sym,u_sym,w_sym,dt);
                %f = Rk4(x_sym,u_sym,w_sym,dt);
              end
              f_fun = casadi.Function('f_fun',{x_sym,u_sym,w_sym},{f});
              A = jacobian(f,x_sym); 
              B = jacobian(f,u_sym); 
              G = jacobian(f,w_sym); 
              A_fun = casadi.Function('A_fun',{x_sym,u_sym,w_sym},{A});
              B_fun = casadi.Function('B_fun',{x_sym,u_sym,w_sym},{B});
              G_fun = casadi.Function('G_fun',{x_sym,u_sym,w_sym},{G});
        end
        function f = dyn(obj,x,u,w,dt)
              f = x+dt* fun(obj,x,u,w);
        end 
        function x_new=multiSim(obj,x,u,w,dt,N_samples)
        %for efficient simulation, implements one simulation step for multiple
        %x,u,w at once;
        %Input:x: n_x\times N_samples; 
        x_new=0*x;
        for i=1:N_samples
             x_new(:,i)=dyn(obj,x(:,i),u(:,i),w(:,i),dt); 
        end
        end
        function w=distribution_worstcase(obj,p,Sigma,N_samples) 
        %generate a matrix of N_sample disturbances;
        %s.t. variance equal Sigma
        %and worst-case for marov
        %
        %worst case: randomly generate 0 with prob. p; 
            %sqrt(Sigma_w/(1-p)) with prob. (1-p)/2
            %-sqrt(Sigma_w/(1-p)) with prob. (1-p)/2
        %... this wouldn't yield Sigma block diagonal for multivariate....
        %1. generate random numbers 0:1
        w_rand=rand(size(Sigma,1),N_samples);
        %w_rand=repmat(w_rand,3,1);
        w=zeros(size(Sigma,1),N_samples).*(w_rand<=p)...
          +sqrtm(Sigma)/sqrt((1-p))*ones(size(Sigma,1),N_samples).*(p<w_rand).*(w_rand<=p+(1-p)/2)...
          -sqrtm(Sigma)/sqrt((1-p))*ones(size(Sigma,1),N_samples).*(w_rand>p+(1-p)/2);
        end
        function w=log_distribution(obj,Sigma,N_samples)
        %compute new variance
        n_w=length(Sigma);
        Sigma=Sigma(1,1);
        %this is tailored to Sigma diagonal
        sigma_log=sqrt(log(0.5+sqrt(0.5^2+Sigma^2)));
        w=mvnrnd(zeros(n_w,1),sigma_log*eye(n_w),N_samples)';
        w=exp(w)-exp(sigma_log/2)*ones(n_w,1);
        end
   end
end
