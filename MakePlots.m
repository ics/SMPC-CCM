%this file generates the plots, given the results saved in "Results"
close all
clear all
clc
Model=Model;%load model
%% plot Friction 
figure(1)
v=0.4*[-1:0.01:1];
for i=1:length(v)
F(i)=Model.F_columb(v(i));
end
plot(v,F,'b-','linewidth',2)  
hold on
plot(v,10*v,'r--','linewidth',2)
ylabel('friction [N]','interpreter','latex')
xlabel('velocity [m/s]','interpreter','latex')
legend({'Coulomb friction','Linear friction'},'interpreter','latex','location','northwest')
axis([min(v),max(v),min(F)*1.1,max(F)*1.1])
set(gca, 'fontname','Arial','fontsize',18*0.45/0.4)
print('Figures/Coulomb','-dpng')
%% plot expected errror
load Results/offline.mat
load Results/PRS_sim.mat
figure(2)
%1. known bound
k=0:T_sim;
semilogy(k.*obj.dt,obj.bar_w/(1-obj.rho)*(1-obj.rho.^(k)),'b-','linewidth',2)
%
%2. samples with different inputs+distributions
hold on  
plot(k.*obj.dt,error_M(:,2,1),'g-','linewidth',2)
plot(k.*obj.dt,error_M(:,1,1),'r-','linewidth',2)
plot(k.*obj.dt,error_M(:,2,2),'g--','linewidth',2)
plot(k.*obj.dt,error_M(:,1,2),'r--','linewidth',2)
plot(k.*obj.dt,error_M(:,1,3),'r:','linewidth',2)
plot(k.*obj.dt,error_M(:,1,4),'r:','linewidth',2)
plot(k.*obj.dt,error_M(:,1,5),'r:','linewidth',2)
plot(k.*obj.dt,error_M(:,1,6),'r:','linewidth',2)
plot(k.*obj.dt,error_M(:,2,3),'g:','linewidth',2)
plot(k.*obj.dt,error_M(:,2,4),'g:','linewidth',2)
plot(k.*obj.dt,error_M(:,2,5),'g:','linewidth',2)
plot(k.*obj.dt,error_M(:,2,6),'g:','linewidth',2)
legend({'Bound', 'With periodic forcing','Without periodic forcing'},'interpreter','latex','location', 'east')

ylabel('${\bf E}[|x-z|_M^2]$','interpreter','latex')
xlabel('t [s]','interpreter','latex')
axis([obj.dt,T_sim*obj.dt,obj.bar_w*0.9,obj.bar_w/(1-obj.rho)*1.1])
axis([obj.dt,50,obj.bar_w*0.9,obj.bar_w/(1-obj.rho)*1.1])
set(gca, 'fontname','Arial','fontsize',18)
print('Figures/Expected_M','-dpng')
%% plot probabiltiy
figure(3)
hold on
stairs(k*obj.dt,(empirical_prob(:,2,1))*1e2,'r','linewidth',2);
stairs(k*obj.dt,(empirical_prob(:,2,7))*1e2,'y--','linewidth',2);
stairs(k*obj.dt,(empirical_prob(:,2,2))*1e2,'g:','linewidth',2);
stairs(k*obj.dt,(empirical_prob(:,2,3))*1e2,'b--','linewidth',2);
stairs(k*obj.dt,(empirical_prob(:,2,4))*1e2,'-.','linewidth',2);
stairs(k*obj.dt,(empirical_prob(:,2,5))*1e2,'m-','linewidth',2);
stairs(k*obj.dt,(empirical_prob(:,2,6))*1e2,'c:','linewidth',2);
plot([0,T_sim]*obj.dt,[1,1]*(obj.probability_level)*1e2,'--black','linewidth',2)
legend({'normal','log-normal','disc. (i)','disc.  (ii)','disc. (iii)','disc. (iv)','disc. (v)'},'interpreter','latex','location', 'southeast') 
ylabel('${\bf Pr}[x(k)\in {\bf R }_k]~[\%]$','interpreter','latex')
xlabel('t [s]','interpreter','latex')
axis([0,20.1,(obj.probability_level-1e-3)*1e2,1e2*1.001])
set(gca, 'fontname','Arial','fontsize',18)
print('Figures/PRS_prob','-dpng') 
%% size of PRS
figure(33)
semilogy([0:T_sim]*obj.dt,obj.bar_w/(1-obj.rho)*(1-obj.rho.^(0:T_sim))/(1-obj.probability_level),'-blue','linewidth',2)
hold on
stairs(k*obj.dt,max(error_percentile(:,2,1),1e-7),'r:','linewidth',1.9);
stairs(k*obj.dt,max(error_percentile(:,2,7),1e-7),'--','linewidth',2.1);
stairs(k*obj.dt,max(error_percentile(:,2,2),1e-7),'g:','linewidth',2);
stairs(k*obj.dt,max(error_percentile(:,2,3),1e-7),'k--','linewidth',2);
stairs(k*obj.dt,max(error_percentile(:,2,4),1e-7),':','linewidth',2);
stairs(k*obj.dt,max(error_percentile(:,2,5),1e-7),'m--','linewidth',2);
stairs(k*obj.dt,max(error_percentile(:,2,6),1e-7),'c:','linewidth',2);
semilogy([0:T_sim]*obj.dt,obj.bar_w/(1-obj.rho)*(1-obj.rho.^(0:T_sim))/(1-obj.probability_level),'-blue','linewidth',2)
%load PRS_few_samples.mat
%stairs(k*obj.dt,max(error_percentile_few_samples(:,2,5),1e-7),'m--','linewidth',2);
%set(gca, 'YScale', 'log')
%legend({'derived bound (Thm. 4)', 'linear approximation', 'nonlinearity bounded', 'normal','disc. (i)','disc.  (ii)','disc. (iii)','disc. (iv)','disc. (v)'},'interpreter','latex','location', 'southeast') 
legend({'Bound','normal','log-normal','disc. (i)','disc.  (ii)','disc. (iii)','disc. (iv)','disc. (v)'},'interpreter','latex','location','south') 
ylabel('95\% Quantile $\|x-z\|_M^2$','interpreter','latex')
xlabel('t [s]','interpreter','latex')
axis([obj.dt,50.1,1e-4,0.045])
set(gca, 'fontname','Arial','fontsize',18) 

print('Figures/PRS_size','-dpng')  
%% Comparison - open loop
load Results/ComparisonSampling.mat
%load Results/Complexity.mat
['Method: ','Nominal ' 'Proposed ', 'bounded', 'sampling: ', num2str(Ns)]
['Comp.-time: ' num2str(t_solve)]
['Probability: ' num2str(Probability)]
['Cost: '    num2str(average_cost)] 
%% Closed-loop cost
load Results/closed_loop.mat
T_plot=22;%over which time to plot (needs to be smaller than closed-loop operation time T_MPC)
figure(4)
k=0:T_MPC;
semilogy(k*obj.dt,cost_closedloop(1:N_samples,:)','r:','linewidth',0.5)
hold on
loglog(k*obj.dt,mean(cost_closedloop(1:N_samples,:)),'b-','linewidth',2)
axis([0,T_plot,1e-7,max(max(cost_closedloop))*1.5])
ylabel('cost $\ell(x,u)$','interpreter','latex')
xlabel('t [s]','interpreter','latex')
set(gca, 'fontname','Arial','fontsize',18)
print('Figures/stage_cost','-dpng') 
%% Plot for closed-loop input
figure(5)
 stairs(k*obj.dt,reshape(u_closedloop(:,1:N_samples,:),[N_samples,T_MPC+1])','r:','linewidth',1)
 hold on
 stairs(k*obj.dt,mean(reshape(u_closedloop(:,1:N_samples,:),[N_samples,T_MPC+1]))','b-','linewidth',2)
 plot([0,max(k)*obj.dt],[1,1]*obj.u_max,'black--','linewidth',2)
 plot([0,max(k)*obj.dt],[1,1]*obj.u_min,'black--','linewidth',2)
axis([0,T_plot,obj.u_min*1.05,obj.u_max*1.05])
ylabel('Actuation $u$ [N]','interpreter','latex')
xlabel('t [s]','interpreter','latex')
set(gca, 'fontname','Arial','fontsize',18)
print('Figures/closedloop_u','-dpng') 
%% Probabilistic constraint
violation=zeros(N_samples,T_MPC+1);
violation_index=zeros(N_samples,T_MPC+1);
for k=1:T_MPC+1
    for i=1:N_samples
    violation(i,k)=max(obj.H*x_closedloop(:,i,k)-obj.h_k(:,1))>0;%check if any constraint is violated
    end
end
max_violation=max(mean(violation,1))%compute probability of violation at each time k (average), then take max for worst case
%%
Hx=zeros(N_samples,T_MPC+1);
for k=1:T_MPC+1
   Hx(:,k)= obj.H(6,:)*x_closedloop(:,1:N_samples,k)-repmat(obj.h_k(6,1),1,N_samples);
end
k=0:T_MPC;
figure(6)
plot(obj.dt*k,-Hx','r:','linewidth',1)
hold on
plot(obj.dt*k,-mean(Hx),'blue-','linewidth',2)
plot(obj.dt*[0,T_MPC+1],0*[1,1],'black--','linewidth',2)
axis([0,T_plot,-0.1,1.3])
ylabel('distance mass 2--3 [m]','interpreter','latex')
xlabel('t [s]','interpreter','latex')
set(gca, 'fontname','Arial','fontsize',18)
print('Figures/closedloop_constraint','-dpng') 


 
