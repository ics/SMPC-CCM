%%
%this file sets up the SMPC and runs closed-loop simulations
clear all
close all
clc
%% set up SMPC
load Results/offline
Model=Model;
import casadi.*
obj.N=15; 
x0=zeros(obj.n_x,1);x0(5)=10;%only large position in last cart
z_init=x0;%initial z at measured
x_init=x0;
%define decision variables
z=MX.sym('z',(obj.N+1)*obj.n_x);%nominal 
x=MX.sym('x',(obj.N+1)*obj.n_x);%mean
u=MX.sym('u',obj.N*obj.n_u);%input
y_dec=[z;x;u];%stacked decision variable
cost_optimized = MX(0);
cost_optimized=costfunction(obj,y_dec);
%define constraints
[c, ceq] = nonlinearconstraints(obj,y_dec,Model); 
con=[ceq;c];
con_bound=zeros(obj.N*obj.n_x*2,1);%dynamic equality
con_lb=[con_bound;-inf(size(obj.h_k,1)*obj.N,1);-inf(1)];
con_ub=[con_bound;reshape(obj.h_k(:,1:obj.N),size(obj.h_k,1)*obj.N,1);0];%mixed inear state inequality constraints + terminal set constraint + 
% set box constraints: [z;x;u]
% input constraints
lb=[-inf(size(z));-inf(size(x));repmat(obj.u_min,obj.N,1)];
ub=[inf(size(z));inf(size(x));repmat(obj.u_max,obj.N,1)];
%add initial state constraint
lb(1:obj.n_x)=z_init;
ub(1:obj.n_x)=z_init;
lb(obj.n_x*(obj.N+1)+1:obj.n_x*(obj.N+1)+obj.n_x)=z_init;
ub(obj.n_x*(obj.N+1)+1:obj.n_x*(obj.N+1)+obj.n_x)=z_init;
%set up NLP
opts=struct;
opts.print_time=0;
opts.ipopt.print_level=0;
%optimization
nlp = struct('x', y_dec, 'f', cost_optimized, 'g', con);
solver = nlpsol('solver', 'ipopt', nlp,opts); 
%%
T_MPC=100;%simulation time
N_samples=1e5;%number of samples (running this many samples at once can take very long, >1day)
x_closedloop=zeros(obj.n_x,N_samples,T_MPC+1);
z_closedloop=zeros(obj.n_x,N_samples,T_MPC+1);
u_closedloop=zeros(obj.n_u,N_samples,T_MPC+1);
x_closedloop(:,:,1)=repmat(x0,1,N_samples);
z_closedloop(:,:,1)=repmat(x0,1,N_samples);
cost_closedloop=zeros(N_samples,T_MPC+1);
cost_cummulated=zeros(N_samples,T_MPC+1);
w_closedloop=zeros(obj.n_w,N_samples,T_MPC);
%% simulation
t=tic;
for i=1:N_samples
%generate disturbances
p_w=0.9995;%specify which discrete distribution
w_closedloop(:,i,:)=Model.distribution_worstcase(p_w,obj.Sigma_w,T_MPC);
%initial guess
y_opt=zeros(size(y_dec));
for k=1:T_MPC
    %re-set initial state constraint + shift tightened constraints
    con_ub=[con_bound;reshape(obj.h_k(:,k:k+obj.N-1),size(obj.h_k,1)*obj.N,1);0];%mixed inear state inequality constraints + terminal set constraint + 
    %set initial state (nominal+real)
    lb(1:obj.n_x)=z_closedloop(:,i,k);
    ub(1:obj.n_x)=z_closedloop(:,i,k);
    lb(obj.n_x*(obj.N+1)+1:obj.n_x*(obj.N+1)+obj.n_x)=x_closedloop(:,i,k);
    ub(obj.n_x*(obj.N+1)+1:obj.n_x*(obj.N+1)+obj.n_x)=x_closedloop(:,i,k); 
    %y_init=y_opt(obj.n_x+1:obj.n_x*(obj.N+1)M
    %'x0' , y_init,... % solution guess     
    y_init=[y_opt(obj.n_x+1:obj.n_x*(obj.N+1));y_opt(obj.n_x*obj.N+1:obj.n_x*(obj.N+1));...
            y_opt(obj.n_x*(1+1+obj.N)+1:obj.n_x*(obj.N+1)*2);y_opt(obj.n_x*(1+obj.N*2)+1:obj.n_x*(obj.N+1)*2);...
            y_opt(obj.n_x*(obj.N+1)*2+obj.n_u+1:end);y_opt(end-obj.n_u+1:end)];
    %y_init=y_opt;
    res = solver(  'x0' , y_init,...
                    'lbx', lb,...           % lower bound on x
                     'ubx', ub,...           % upper bound on x
                     'lbg', con_lb,...           % lower bound on g
                     'ubg', con_ub);             % upper bound on g
    y_opt=full(res.x); 
    %check feasibility
    [c_test,c_eq_test]=nonlinearconstraints(obj,y_opt,Model);
    if norm(c_eq_test)>1e-6 | max(c_test-con_ub(length(c_eq_test)+1:end))>1e-6 %check dynamics + inequality constraints (not all inequality are coded as <=c) 
         [k,norm(c_eq_test),max(c_test-con_ub(length(c_eq_test)+1:end))]
         error('MPC infeasible')
    end
    u_closedloop(:,i,k)=y_opt((obj.N+1)*obj.n_x*2+1:(obj.N+1)*obj.n_x*2+obj.n_u);
    z_closedloop(:,i,k+1)=Model.dyn(z_closedloop(:,i,k),u_closedloop(:,i,k),0*w_closedloop(:,i,k),obj.dt);
    x_closedloop(:,i,k+1)=Model.dyn(x_closedloop(:,i,k),u_closedloop(:,i,k),w_closedloop(:,i,k),obj.dt);
    cost_closedloop(i,k)=runningcosts(x_closedloop(:,i,k), u_closedloop(:,i,k), 0*x_closedloop(:,i,k), 0*u_closedloop(:,i,k),obj.Q, obj.R);
    cost_cummulated(i,k+1)=cost_cummulated(i,k)+cost_closedloop(i,k);
end
end
toc(t)
%% save result
save('closed_loop','cost_closedloop','u_closedloop','x_closedloop','N_samples','T_MPC')

function cost=costfunction(param,y_dec)
        %open-loop cost for decision variable y_dec
        x=y_dec((param.N+1)*param.n_x+1:(param.N+1)*param.n_x*2);
        u=y_dec((param.N+1)*param.n_x*2+1:end);
         cost = 0;
        for k=1:param.N
           x_k=x(param.n_x*(k-1)+1:param.n_x*k);
           u_k=u(param.n_u*(k-1)+1:param.n_u*k);
           cost = cost + runningcosts(x_k, u_k,0*x_k,0*u_k,param.Q, param.R); 
        end
        %terminal:
           x_N=x(param.n_x*param.N+1:param.n_x*(param.N+1));
           cost = cost + runningcosts(x_N, 0,0*x_N,0,param.P,0); 
end

function cost = runningcosts(x, u, x_eq, u_eq, Q, R)
            % Provide the running cost   
            cost = (x-x_eq)'*Q*(x-x_eq) + (u-u_eq)'*R*(u-u_eq);
end

function [c, ceq] = nonlinearconstraints(param,y_dec,Model) 
        % Introduce the nonlinear constraints also for the terminal state  
        z=y_dec(1:(param.N+1)*param.n_x);
        x=y_dec((param.N+1)*param.n_x+1:(param.N+1)*param.n_x*2);
        u=y_dec((param.N+1)*param.n_x*2+1:end);
        c = [];
        ceq = [];  
        %last variable is nominal initial state
        %-> make x trajectory nominal 
        c = [];
        ceq = [];
        % constraints along prediction horizon
            for k=1:param.N
                x_k=x(param.n_x*(k-1)+1:param.n_x*k);
                u_k=u(param.n_u*(k-1)+1:param.n_u*k);
                x_new=x(param.n_x*k+1:param.n_x*(k+1));
                z_k=z(param.n_x*(k-1)+1:param.n_x*k);
                z_new=z(param.n_x*k+1:param.n_x*(k+1));
                %dynamic constraint x,z:
                ceqnew=x_new - Model.dyn(x_k, u_k,zeros(param.n_w,1),param.dt);
                ceq = [ceq; ceqnew];
                ceqnew=z_new - Model.dyn(z_k, u_k,zeros(param.n_w,1),param.dt);
                ceq = [ceq; ceqnew];
                %nonlinear constraints on state and input could be included here 
                cnew=param.H*z_k;%(linear) state constraints (not just box, due to difference position)
                c=[c;cnew];
            end  
             %terminal set constraint: |z_N|_M^2\leq \alpha_f
             z_N=z(param.n_x*param.N+1:param.n_x*(param.N+1));
            cnew=[runningcosts(z_N,0,0*z_N,0,param.M,0)-param.alpha_f];%<=0 ensures terminal
            c=[c;cnew];
end
  
        
        